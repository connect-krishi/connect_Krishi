import 'package:flutter/material.dart';
import 'package:login_signup/comm/genTextFormField.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Login')),
        backgroundColor: Colors.green, // set app bar background color
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Center(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const SizedBox(
                  height: 40.0,
                ),
                const Text(
                  "Please Login to Your Account",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Image.asset(
                  "assets/img/login.jpg",
                  height: 200.0,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                const Text(
                  "Welcome, Aneel",
                  style: TextStyle(
                    // fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 155, 155, 155),
                    fontSize: 28.0,
                  ),
                ),
                getTextformField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter your UserName';
                    }
                    return null;
                  },
                  controller: _usernameController,
                  hintName: 'User Name',
                  icon: Icons.person,
                ),
                getTextformField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter your Password';
                    }
                    return null;
                  },
                  controller: _passwordController,
                  hintName: 'PassWord',
                  icon: Icons.key,
                  isObscureText: true,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        Navigator.pushReplacementNamed(context, '/home');
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor:
                          Colors.green, // set button background color
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      padding: const EdgeInsets.all(0),
                      elevation: 5.0,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 48.0, vertical: 12.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: const <Widget>[
                          Icon(
                            Icons.lock_outline,
                            color: Colors.white,
                          ),
                          SizedBox(width: 8.0),
                          Text(
                            'Sign In',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed('/signup');
                    
                  },
                  child: const Text('Create an account',
                      style: TextStyle(color: Colors.green)), // set text color
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
