import 'package:flutter/material.dart';
import '../comm/dashboard_tile.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOut,
    );
    _playAnimation();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _playAnimation() {
    _controller.forward(from: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
        backgroundColor: Colors.green[800],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(16.0),
            color: Colors.green[500],
            child: FadeTransition(
              opacity: _animation,
              child: const Text(
                'Welcome to the Agriculture Marketplace!',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Expanded(
            child: GridView.count(
              padding: const EdgeInsets.all(16.0),
              crossAxisCount: 2,
              children: <Widget>[
                ScaleTransition(
                  scale: _animation,
                  child: DashboardTile(
                    icon: Icons.search,
                    title: 'Search Listings',
                    onTap: () {
                      // Navigate to the search screen
                    },
                  ),
                ),
                ScaleTransition(
                  scale: _animation,
                  child: DashboardTile(
                    icon: Icons.add_circle_outline,
                    title: 'Create Listing',
                    onTap: () {
                      // Navigate to the create listing screen
                    },
                  ),
                ),
                ScaleTransition(
                  scale: _animation,
                  child: DashboardTile(
                    icon: Icons.shopping_cart,
                    title: 'My Orders',
                    onTap: () {
                      // Navigate to the orders screen
                    },
                  ),
                ),
                ScaleTransition(
                  scale: _animation,
                  child: DashboardTile(
                    icon: Icons.person_outline,
                    title: 'My Profile',
                    onTap: () {
                      // Navigate to the profile screen
                    },
                  ),
                ),
                ScaleTransition(
                  scale: _animation,
                  child: DashboardTile(
                    icon: Icons.settings,
                    title: 'Settings',
                    onTap: () {
                      // Navigate to the settings screen
                    },
                  ),
                ),
                ScaleTransition(
                  scale: _animation,
                  child: DashboardTile(
                    icon: Icons.help_outline,
                    title: 'Help & Support',
                    onTap: () {
                      // Navigate to the help & support screen
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
