import 'package:flutter/material.dart';

import '../comm/genTextFormField.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({super.key});

  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const SizedBox(
                  height: 40.0,
                ),
                const Text(
                  "Please SignUp",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Image.asset(
                  "assets/img/signup.jpg",
                  height: 200.0,
                ),
                const SizedBox(
                  height: 10.0,
                ),


                getTextformField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter your Name';
                    }
                    return null;
                  },
                  controller: _nameController,
                  hintName: 'Full Name',
                  icon: Icons.person,
                  isObscureText: true,
                ),


                getTextformField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter your Email Address';
                    }
                    return null;
                  },
                  controller: _emailController,
                  hintName: 'Email Address',
                  icon: Icons.mail,
                  isObscureText: false,
                ),


                 getTextformField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter your Password';
                    }
                    return null;
                  },
                  controller: _passwordController,
                  hintName: 'PassWord',
                  icon: Icons.key,
                  isObscureText: true,
                ),


                getTextformField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter your Password';
                    }
                    return null;
                  },
                  controller: _passwordController,
                  hintName: 'Confirm PassWord',
                  icon: Icons.vpn_key,
                  isObscureText: true,
                ),


                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () {
                      try {
                        if (_formKey.currentState!.validate()) {
                          Navigator.of(context).pushNamed('/login');
                        }
                      } catch (e) {
                        print('Error: $e');
                        // handle the exception here, e.g. show an error message to the user
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      padding: const EdgeInsets.all(0),
                      elevation: 5.0,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 48.0, vertical: 12.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: const <Widget>[
                          Icon(
                            Icons.lock_outline,
                            color: Colors.white,
                          ),
                          SizedBox(width: 8.0),
                          Text(
                            'Sign Up',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
