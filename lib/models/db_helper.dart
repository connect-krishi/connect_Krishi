import 'dart:io';
import "package:path/path.dart" show join;
import 'package:login_signup/models/user.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBHelper {
  static final DBHelper _instance = DBHelper._internal();
  factory DBHelper() => _instance;

  static Database? _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db!;
    }
    _db = await initDB();
    return _db!;
  }

  DBHelper._internal();

  Future<Database> initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "my_database.db");
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE users(id INTEGER PRIMARY KEY, username TEXT, email TEXT, password TEXT)");
  }

  Future<int> insertUser(User user) async {
    Database db = await this.db;
    return await db.insert('users', user.toMap());
  }

  Future<User?> getUser(String email, String password) async {
    Database db = await this.db;
    List<Map<String, dynamic>> maps = await db.query('users',
        where: "email = ? AND password = ?", whereArgs: [email, password]);
    if (maps.isNotEmpty) {
      return User.fromMap(maps.first);
    }
    return null;
  }
}
