

import 'package:flutter/material.dart';

class getTextformField extends StatelessWidget {
 
  TextEditingController controller;
  String hintName;
  IconData icon;
  bool isObscureText;

  getTextformField({
    Key? key,
    required this.controller,
    required this.hintName,
    required this.icon,
    this.isObscureText = false, required String? Function(dynamic value) validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      margin: const EdgeInsets.only(top: 15.0),
      child: TextFormField(
        controller: controller,
        obscureText: isObscureText,
        decoration: InputDecoration(
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
            borderSide: BorderSide(color: Colors.blue),
          ),
          prefixIcon: Icon(icon),
          hintText: hintName,
          fillColor: const Color.fromARGB(255, 230, 230, 230),
          filled: true,
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
          ),
        ),
      ),
    );
  }
}
